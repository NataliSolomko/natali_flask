import os
basedir = os.path.abspath(os.path.dirname(__file__))
APP_PATH = os.path.dirname(__file__)


class Config(object):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///%s/database.db?check_same_thread=False' % APP_PATH
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = b'\xea\x0bl>}\x8c\xe5\xac\x05\xac\xdf\tbNQ\t'
    MAIL_SERVER = 'smtp.googlemail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = 1
    MAIL_USERNAME = 'natalitest0@gmail.com'
    MAIL_PASSWORD = 'qwerty123=!'
    ADMINS = ['natalitest0@gmail.com']
    POSTS_PER_PAGE = 10
    PORT = 8080
    LANGUAGES = ['en', 'ru', 'uk']
