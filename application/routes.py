from flask import render_template, flash, redirect, request, url_for
from flask_login import login_user, login_required, current_user, logout_user
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.urls import url_parse
from datetime import datetime

from application import app, models, db, login_manager
from application.models import User
from .forms import LoginForm, RegForm, EditProfileForm, PostForm, ResetPasswordRequestForm, ResetPasswordForm


def pagination(posts_in, page_link, username=None):
    with_user = ['user', ]
    page = request.args.get('page', 1, type=int)
    posts = posts_in.paginate(page, app.config['POSTS_PER_PAGE'], False)
    if page_link in with_user:
        next_url = url_for(page_link, username=username, page=posts.next_num) if posts.next_num else None
        prev_url = url_for(page_link, username=username, page=posts.prev_num) if posts.prev_num else None
    else:
        next_url = url_for(page_link, page=posts.next_num) if posts.next_num else None
        prev_url = url_for(page_link, page=posts.prev_num) if posts.prev_num else None
    return posts.items, next_url, prev_url



@login_manager.user_loader
def load_user(user_id):
    return models.User.query.get(user_id)


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    # user = {'username': models.User.query.filter_by(username="Pashtet").first().username}
    form = PostForm()
    if form.validate_on_submit():
        post = models.Post(body=form.post.data, author=current_user)
        db.session.add(post)
        db.session.commit()
        flash('You have created a new post!')
        return redirect('index')
    posts = current_user.followed_posts()
    posts_items, next_url, prev_url = pagination(posts, 'index')
    return render_template('index.html', title="Main page", posts=posts_items, form=form, next_url=next_url,
                           prev_url=prev_url)


@app.route('/explore')
@login_required
def explore():
    posts = models.Post.query.order_by(models.Post.timestamp.desc())
    posts_items, next_url, prev_url = pagination(posts, 'explore')
    return render_template('index.html', title="Explore", posts=posts_items, next_url=next_url, prev_url=prev_url)


@app.route('/secret_page')
@login_required
def secret_page():
    return "this is SECRET PAGE"


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        username = form.data.get("username")
        password = form.data.get("password")
        user = models.User.query.filter_by(username=username).first() or None
        if user and check_password_hash(user.password, password):
            user.authenticated = True

            db.session.add(user)
            db.session.commit()
            login_user(user)

            next_page = request.args.get('next')
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_parse('/index')
            return redirect(next_page)
        else:
            flash('Invalid username or password')
            return redirect(url_for('index'))
    return render_template('login.html', title='Sign In', form=form)


@app.route('/reg', methods=['GET', 'POST'])
def reg():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegForm()
    if form.validate_on_submit():
        username = form.data.get("username")
        password = form.data.get("password")
        email = form.data.get("email")
        hashed_pwd = generate_password_hash(password)

        new_user = models.User(username=username, password=hashed_pwd, email=email, authenticated=True)

        db.session.add(new_user)
        db.session.commit()

        login_user(new_user)
        return redirect(url_for('index'))
    return render_template('reg.html', title='Sign In', form=form)


@app.route('/logout')
@login_required
def logout():
    current_user.authenticated = False
    db.session.add(current_user)
    db.session.commit()
    logout_user()
    return redirect(url_for('index'))


@app.route('/user/<username>')
@login_required
def user(username):
    user = models.User.query.filter_by(username=username).first_or_404()
    user_id = user.get_id()
    posts = models.Post.query.filter_by(user_id=user_id).order_by(models.Post.timestamp.desc())
    posts_items, next_url, prev_url = pagination(posts, 'user', username=username)
    return render_template('user.html', user=user, posts=posts_items, next_url=next_url, prev_url=prev_url)


@app.route('/follow/<username>')
@login_required
def follow(username):
    user = models.User.query.filter_by(username=username).first()
    current_user.follow(user)
    db.session.commit()
    flash(f'You are following {username}!')
    return redirect(url_for('user', username=username))


@app.route('/unfollow/<username>')
@login_required
def unfollow(username):
    user = models.User.query.filter_by(username=username).first()
    current_user.unfollow(user)
    db.session.commit()
    flash(f'You are not following {username}!')
    return redirect(url_for('user', username=username))


@app.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()


@app.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm()
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
        return  render_template('edit_profile.html', title='Edit Profile', form=form)


@app.route('/reset_password_request', methods=['GET', 'POST'])
def reset_password_request():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = ResetPasswordRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()

        if user:
            form.send_password_reset_email(user)
        flash('Check your email for the instructions to reset your password')
        return redirect(url_for('login'))
    return render_template('reset_password_request.html', title='Reset Password', form=form)


@app.route('/reset_password/<token>', methods=['GET', 'POST'])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    user = User.verify_reset_password_token(token)
    if not user:
        flash('Token has been expired!')
        return redirect(url_for('login'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        db.session.commit()
        flash('Your password has been reset.')
        return redirect(url_for('login'))
    return render_template('reset_password.html', form=form)