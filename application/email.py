from flask_mail import Message
from application import mail, app
from threading import Thread


def send_email(subject, sender, recipients, text_body, html_body, attachments):
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    if attachments:
        for a in attachments:
            with open(a, 'r') as atmt:
                msg.attach(a, "text/plain", atmt.read())
    mail.send(msg)
    Thread(target=send_async_email, args=(app, msg)).start()

def send_async_email(app, msg):
        with app.app_context():
            mail.send(msg)