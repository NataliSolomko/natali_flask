from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Length, Email, EqualTo
from flask import render_template
from application import app
from application.email import send_email
from application.config import APP_PATH


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Log In')


class RegForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired()])
    submit = SubmitField('Sign In')


class EditProfileForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    about_me = TextAreaField('About me', validators=[Length(min=0, max=150)])
    submit = SubmitField('Submit')


class PostForm(FlaskForm):
    post = TextAreaField('Create your new post!', validators=[DataRequired(), Length(min=1, max=150)])
    submit = SubmitField('Submit')


class ResetPasswordRequestForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Request Password Reset')

    def send_password_reset_email(self, user):
        token = user.get_reset_password_token()
        attachment_path = 'C:\\Users\\admin\\PycharmProjects\\natali_flask\\logs\\microblog.log'
        send_email('[Microblog] Reset Your Password',
                   sender=app.config['ADMINS'][0],
                   recipients=[user.email],
                   text_body=render_template('email/reset_password.txt', user=user, token=token),
                   attachments=[attachment_path],
                   html_body=render_template('email/reset_password.html', user=user, token=token))


class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Request Password Reset')