from flask import render_template

from application import db, app
import datetime
from hashlib import md5
from time import time
import jwt
from application.email import send_email
from werkzeug.security import generate_password_hash



ROLE_USER = 0
ROLE_ADMIN = 1

followers = db.Table('followers',
    db.Column('follower_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('followed_id', db.Integer, db.ForeignKey('user.id'))
)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True)
    email = db.Column(db.String(120))
    password = db.Column(db.String(64))
    authenticated = db.Column(db.Boolean(), default=False)
    role = db.Column(db.SmallInteger, default=ROLE_USER)
    about_me = db.Column(db.String(150))
    last_seen = db.Column(db.DateTime, default=datetime.datetime.utcnow())
    posts = db.relationship('Post', backref='author', lazy='select')
    followed = db.relationship(
        'User', secondary=followers,
        primaryjoin=(followers.c.follower_id == id),
        secondaryjoin=(followers.c.followed_id == id),
        backref=db.backref('followers', lazy='select'), lazy='select'
    )

    def __init__(self, username, password, email, authenticated=False, role=''):
        self.username = username
        self.password = password
        self.email = email
        self.authenticated = authenticated
        self.role = role

    def __repr__(self):
        return '<User %s>' % (self.username)

    def is_authenticated(self):
        return self.authenticated

    def is_active(self):
        return True

    def get_id(self):
        return str(self.id)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=monsterid&s={}'.format(digest, size)

    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)

    def is_following(self, user):
        return self.followed.count(user) > 0

    def count_followers(self):
        return len(self.followers)

    def count_followed(self):
        return len(self.followed)

    def followed_posts(self):
        return Post.query.join(
            followers, (followers.c.followed_id == Post.user_id)).filter(
            followers.c.follower_id == self.id).order_by(
            Post.timestamp.desc())

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            app.config['SECRET_KEY'], algorithm='HS256')

    def send_password_reset_email(user):
        token = user.get_reset_password_token()
        send_email('[Microblog] Reset Your Password',
                   sender=app.config['ADMINS'][0],
                   recipients=[user.email],
                   text_body=render_template('email/reset_password.txt',
                                             user=user, token=token),
                   html_body=render_template('email/reset_password.html',
                                             user=user, token=token))

    @staticmethod
    def verify_reset_password_token(token):
        try:
            id = jwt.decode(token, app.config['SECRET_KEY'],
                            algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(500))
    timestamp = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __init__(self, body, author):
        self.body = body
        self.author = author
        self.timestamp = datetime.datetime.utcnow()

    def __repr__(self):
        return '<Post %s>' % (self.body)
