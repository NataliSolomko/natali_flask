from application import models

all_users = [i for i in models.User.query.all()]
all_posts = [i for i in models.Post.query.all()]


def main():
    print("""#################\nUSERS\n###############\n""")
    for item in all_users:
        print(item.id, item.username)

    print("""#################\nPosts\n###############""")

    for post in all_posts:
        print(post.id, post.author, post.body)


if __name__ == '__main__':
    main()
