from application import models


def find_user_posts(name, mode="all"):
    _user_id = models.User.query.filter_by(username=name).first().id
    if mode == 'all':
        _post_list = models.Post.query.filter_by(user_id=_user_id).all()
        for i in _post_list:
            print(i.author, i.timestamp, i.body)
    else:
        _post_list = models.Post.query.filter_by(user_id=_user_id).first()
        print(_post_list.author, _post_list.timestamp, _post_list.body)
